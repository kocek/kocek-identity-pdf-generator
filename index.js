const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const moment = require('moment');
const pdf = require('html-pdf');
const tmpl = fs.readFileSync(require.resolve('./template.html'), 'utf8');

require('dotenv').config();
const app = express();
const PORT = process.env.PORT || 5001;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/create', async (req, res) => {
  try {
    const { identityNumber } = req.body;
    const html = mapParamsToTemplate(tmpl, req.body);
    const pdfResult = await createPdf(html, identityNumber);

    res.status(200).send(pdfResult);
  } catch (error) {
    console.log(error);
  }
});

app.get('/download/:fileName', (req, res, next) => {
  const { fileName } = req.params;
  const options = {
    root: __dirname + '/output/',
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true,
    }
  };

  res.sendFile(fileName, options, (err) => {
    if (err) next(err);
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

const mapParamsToTemplate = (template, {
  name,
  identityNumber,
  phoneNumber,
  job,
  loanNominal,
  loanType,
  identityCardImage,
  familyCardImage,
}) => {
  let HTMLResult;

  HTMLResult = template.replace('{{ name }}', name);
  HTMLResult = HTMLResult.replace('{{ identityNumber }}', identityNumber);
  HTMLResult = HTMLResult.replace('{{ phoneNumber }}', phoneNumber);
  HTMLResult = HTMLResult.replace('{{ job }}', job);
  HTMLResult = HTMLResult.replace('{{ loanNominal }}', loanNominal);
  HTMLResult = HTMLResult.replace('{{ loanType }}', loanType);
  HTMLResult = HTMLResult.replace('{{ identityCardImage }}', identityCardImage);
  HTMLResult = HTMLResult.replace('{{ familyCardImage }}', familyCardImage);

  return HTMLResult;
};

const createPdf = (template, identityNumber) => new Promise((resolve, reject) => {
  const fileName = `${moment().format('YYYYMMDD-kkmmss')}-${identityNumber}.pdf`;

  pdf.create(template, {
    width: '21cm',
    height: '29.7cm'
  }).toFile(`./output/${fileName}`, (err, { filename: path }) => {
    if (err) {
      reject(err);
    }

    resolve({
      fileName,
      path,
    });
  });
});
